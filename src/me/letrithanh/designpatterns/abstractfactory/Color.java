package me.letrithanh.designpatterns.abstractfactory;

public interface Color {
	public void fill();
}
