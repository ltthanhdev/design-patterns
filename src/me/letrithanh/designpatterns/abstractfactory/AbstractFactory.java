package me.letrithanh.designpatterns.abstractfactory;

import me.letrithanh.designpatterns.factory.Shape;

public abstract class AbstractFactory {
	
	public abstract Shape getShape(String shapeType);
	
	public abstract Color getColor(String color);
	
}
