package me.letrithanh.designpatterns.abstractfactory;

public class AbstractFactoryDemo {

	public static void main(String[] args) {
		FactoryProducer.getFactory("shape").getShape("circle").draw();
		FactoryProducer.getFactory("color").getColor("red").fill();
	}

}
