package me.letrithanh.designpatterns.abstractfactory;

import me.letrithanh.designpatterns.factory.Circle;
import me.letrithanh.designpatterns.factory.Rectangle;
import me.letrithanh.designpatterns.factory.Shape;
import me.letrithanh.designpatterns.factory.Square;

public class ShapeFactory extends AbstractFactory {

	@Override
	public Shape getShape(String shapeType) {
		if(shapeType.equalsIgnoreCase("circle")) {
			return new Circle();
		}
		
		if(shapeType.equalsIgnoreCase("square")) {
			return new Square();
		}
		
		if(shapeType.equalsIgnoreCase("rectangle")) {
			return new Rectangle();
		}
		
		return null;
	}

	@Override
	public Color getColor(String color) {
		return null;
	}

}
