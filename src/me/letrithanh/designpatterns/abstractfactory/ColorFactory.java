package me.letrithanh.designpatterns.abstractfactory;

import me.letrithanh.designpatterns.factory.Shape;

public class ColorFactory extends AbstractFactory {
	
	@Override
	public Color getColor(String color) {
		if(color.equalsIgnoreCase("red")) {
			return new Red();
		}
		
		if(color.equalsIgnoreCase("blue")) {
			return new Blue();
		}
		
		if(color.equalsIgnoreCase("green")) {
			return new Green();
		}
		
		return null;
	}

	@Override
	public Shape getShape(String shapeType) {
		return null;
	}
	
}
