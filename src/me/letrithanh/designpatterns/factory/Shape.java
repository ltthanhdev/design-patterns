package me.letrithanh.designpatterns.factory;

public interface Shape {
	public void draw();
}
