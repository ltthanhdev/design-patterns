package me.letrithanh.designpatterns.factory;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("Circle::draw()");		
	}

}
