package me.letrithanh.designpatterns.factory;

public class FactoryPattern {

	public static void main(String[] args) {
		ShapeFactory shapeFactory = new ShapeFactory();
		shapeFactory.getShape("circle").draw();
		shapeFactory.getShape("square").draw();
		shapeFactory.getShape("rectangle").draw();
	}

}
